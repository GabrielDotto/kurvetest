import { Component, OnInit } from '@angular/core';
import * as kurve from 'kurvejs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('init');
    this.login();
    console.log('KURVE', kurve);
  }

  login() {
    const identity = new kurve.Identity('47f2c763-ded3-495c-8b9e-fd3511a56b4b', 'http://localhost:4200/permissions');

    console.log('IDENTITY ', identity);

    identity.loginAsync().then(_ => {
      console.log('LOGIN ASYNC');
    });

  }

}
